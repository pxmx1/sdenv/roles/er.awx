Role Name
=========

Installiert AWX21+ auf einem k3s Master. 
Konfiguriert AWX mit LDAP an ipa.dns.local und Logging an elastic-1.dns.local

Requirements
------------

Collections: awx.awx 

Roles:  lra.k3s


Role Variables
--------------

Kubernetes:

Variable | Description | Default
-------- | ----------- | -------
`awx_namespace`| Kubernetes Namespace | `awx`
`awx_admin_username`| AWX Admin Username | `password`
`awx_admin_password`| AWX Admin User Password | `password`
`awx_nodeport`| AWX Node Port | `30080`

AWX LDAP Config:

Variable | Description | Default
-------- | ----------- | -------
`base_dn`| LDAP Base DN | `dc=my,dc=lan`
`awx_ldap_server_uri`| LDAP Server URL | `ldap://ipa.my.lan:389`
`awx_ldap_bind_dn`| LDAP Bind User | `uid=awx_bind,cn=users,cn=accounts,{{ base_dn }}`
`awx_ldap_bind_dn_password`| LDAP Bind User Password | `password`
`awx_ldap_user_search_base`| LDAP User Search Base | `cn=users,cn=accounts,{{ base_dn }}`
`awx_ldap_group_search_base`| LDAP Group Search Base | `cn=groups,cn=accounts,{{ base_dn }}`
`awx_admin_group_dn`| AWX Admin Group DN | `cn=awx-admins,{{ awx_ldap_group_search_base }}`
`awx_user_group_dn`| AWX User Group DN | `cn=awx-users,{{ awx_ldap_group_search_base }}`
`awx_auditor_group_dn`| AWX Auditor Group DN | `cn=awx-auditors,{{ awx_ldap_group_search_base }}`
`awx_org2user_group_dn`| AWX Org2 User Group DN | `cn=awx-org2users,{{ awx_ldap_group_search_base }}`
`awx_organization`| AWX Organisation | `meinefirma`
`awx_config_importfile`| Filename zum Import einer bestehenden AWX Resource & Access Konfiguration | `import.json`

AWX Logging:

Variable | Description | Default
-------- | ----------- | -------
`awx_logging_aggregator`| Logstash logging server. Hostname or IP. |`elastic.my.lan`
`awx_logging_port`| logstash listening port | `5005`

Misc:

Variable | Description | Default
-------- | ----------- | -------
`awx_controller_url`|  | `http://awx.my.lan:{{ awx_nodeport }}`


Dependencies
------------

-

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Deploy a Ubuntu Cloud Image Virtual Appliance AWX Server
      hosts: awx
      gather_facts: false
      
      roles:
        - role: lra.vmware_ubuntu_cloud_image
          vars:
            annotation: '{{inventory_hostname}} - AWX Server - automated installation'
            disk:
              - size_gb: 30
                datastore: san-nfs
                scsi_controller: 0
                unit_number: 0
            hardware:
              num_cpus: 8
              memory_mb: 12288

        - role: lra.ubuntu
        - role: lra.ipa_client

        - role: lra.k3s

        - role: lra.awx
          become: true
          tags: awx


License
-------

BSD

Author Information
------------------

Engelbert Roidl